// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function(){

	$('#buy').on('click', function(){
		OpenInNewTab();
	});

	$('#main_form').on('valid.fndtn.abide', function() {
		// disable submit button
		$('#main_form .submit_btn').prop('disabled', true);
		// save form data
	  	saveForm('#main_form');
	});

	$('#modal_form').on('valid.fndtn.abide', function() {
		// disable submit button
		$('#modal_form .submit_btn').prop('disabled', true);
		// save form data
	  	saveForm('#modal_form');
	});

	function OpenInNewTab() {
	  var win = window.open('http://buy.thewitcher.com/', '_blank');
	  win.focus();
	}


	var saveForm = function(formType){

	        var data = $( formType ).serialize();

	        $.ajax({
	            url: "ajax.php",
	            type: "POST",
	            data: data,
	            /**
	             * Function called if the request succeeds
	             * @method success
	             * @param {Object} d
	             */
	            success: function(response, textStatus, jqXHR) {

	            	if(response == 1){
	            		// success saving form data
	            		$( formType+" .forminput" ).val("");
	            		$( formType+" .thankyou-modal" ).fadeIn(1000);
	            		$( formType+" .row").animate({
	            			opacity : 0.6
	            		}, 300, 'swing');

	            	}else{
	            		// error saving form data
	            		alert('There was an error saving your data, please try again shortly.');

	            	}

	            	$(formType+' .submit_btn').prop('disabled', false);

	            },
	            /**
	             * Function called if the request failed
	             * @method error
	             * @param {jqXHR} jqXHR
	             * @param {String} status
	             * @param {String} errorThrown
	             */
	            error: function(jqXHR, status, errorThrown) {

	            	alert('There was an error saving your data, please try again shortly.');
	            	$(formType+' .submit_btn').prop('disabled', false);

	            }

	        });

	} // end save function


	$('#stick').waypoint('sticky');

	$('#signup').on('click', function(){
		$(window).scrollTo('#comp', 800, 'swing');
	});

	//about the game carousel
	$('#slider').slick({
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    centerPadding: '100px',
	    dots: true,
	    centerMode: true,
	    focusOnSelect: true
	});


	function makeActive(target){
		var el = target;

		el.parent().parent().find('li').each(function(){
			if($(this).find('a').hasClass('active')) {
				$(this).find('a').removeClass('active');
			}
		});

		el.addClass('active');
	}

	$('.menu-icon').on('click', function(){
		$('.off-canvas-wrap').foundation('offcanvas', 'toggle', 'move-right');
	});

	$('.choose').on('click', function(){

		makeActive($(this));
		$(window).scrollTo('#movies', 500, 'swing');
		$('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-right');

	});

	$('.join').on('click', function(){
		makeActive($(this));
		$(window).scrollTo('#comp', 500, 'swing');
		$('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-right');

	});

	$('.about').on('click', function(){
		makeActive($(this));
		$(window).scrollTo('.aboutthegame', 500, 'swing');
		$('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-right');

	});

	if($('#no_age_cookie').length) {
		$('#no_age_cookie').appendTo('#start_modal').css('visibility', 'visible');
		$('#start_modal').foundation('reveal', 'open');
		$('.chosen-container').css('width', '31.5%');
	}


	new Share(".share_btn", {
		ui: {
		  flyout: "bottom center" 
		},
		networks: {
		    google_plus: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/thewitcher3/"
		    },
		    twitter: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/thewitcher3/",
		      description:    "Vote for your favourite monster movie so @IGNUK and @witchergame can put on a special screening for the winning film!"
		    },
		    facebook: {
		      enabled: true,
		      load_sdk: true,// Load the FB SDK. If false, it will default to Facebook's sharer.php implementation. 
		                // NOTE: This will disable the ability to dynamically set values and rely directly on applicable Open Graph tags.
		                // [Default: true]
		      url: "http://uk-microsites.ign.com/thewitcher3/",
		      //app_id: "551643344962507",
		      title: "Vote for your favourite monster movie with IGN and The Witcher 3",
		      caption: "Vote for your favourite monster movie with IGN and The Witcher 3",
		      description:    "Vote for your favourite monster movie so @IGNUK and @witchergame can put on a special screening for the winning film!"
		      //image: // image to be shared to Facebook [Default: config.image]
		    },
		    pinterest: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/thewitcher3/",
		      //image:   // image to be shared to Pinterest [Default: config.image]
		      description:    "Vote for your favourite monster movie so @IGNUK and @witchergame can put on a special screening for the winning film!"
		    },
		    email: {
		      enabled: true,
		      title:     "Vote for your favourite monster movie with IGN and The Witcher 3",
		      description:    "Vote for your favourite monster movie so @IGNUK and @witchergame can put on a special screening for the winning film!"
		    }
		  }

	});
});



/**angular**/

var evilapp = angular.module('EvilApp', []);

evilapp.controller('AppCtrl', function($scope){
	var app = this;

	app.body = $scope;

	app.message = "Vote";
	app.class = "inactive";



});

evilapp.directive('spinbackDirective', function(){
	return function (scope, element, attrs) { //grab the element

		restrict: "A",

		element.on('click', function(){

			element.parents('.flip-container').removeClass('flip');

			$('#movies').find('.large-3').each(function(){
					$(this).animate({
						opacity: 1
					}, 400, 'swing', function(){
						//$(window).scrollTo('#submit', 800, 'swing', {offset:-100});
						$(this).find('p').removeClass('dontClick');
					});
			});

			element.parent().prev().find('p').removeClass('selected').text(scope.app.message);
			element.parent().prev().find('p').next().removeClass('selected');


		});

	}
});

evilapp.directive('voteDirective', function(){
	return function (scope, element, attrs) { //grab the element

		restrict: "A",

		element.next().on('click', function(){

			if (element.hasClass('dontClick')) {
		        return false;
		     }

			//if($('#movies').hasClass('active') !== true) {

				if(element.hasClass(attrs.activate) !== true) {
					element.text(attrs.activate).addClass(attrs.activate);
					element.text(attrs.activate).next().addClass(attrs.activate);


					$('#movies').addClass('active');

					//if one hasnt been selected
					$('#movies').find('.large-3').each(function(){
						if($(this).find('p').hasClass('selected') !== true) {

							$(this).animate({
								opacity: 0.3 
							}, 400, 'swing', function(){
								
								$(this).find('p').addClass('dontClick');

							});



						} else {
							//element.removeClass(attrs.activate).text(scope.app.message);
							
						}
					});

					function flip(){
						
						element.parents('.flip-container').addClass('flip');
					}
					

					setTimeout(flip, 400)


				} else {

					element.removeClass(attrs.activate).text(scope.app.message);
					element.next().removeClass(attrs.activate);

					//if one has been selected
					$('#movies').find('.large-3').each(function(){
							$(this).animate({
								opacity: 1
							}, 400, 'swing', function(){
								//$(window).scrollTo('#submit', 800, 'swing', {offset:-100});
								$(this).find('p').removeClass('dontClick');
							});
					});
				}

			//}

		
		});

		element.on('click', function(){

			if (element.hasClass('dontClick')) {
		        return false;
		     }

			//if($('#movies').hasClass('active') !== true) {

				if(element.hasClass(attrs.activate) !== true) {
					element.text(attrs.activate).addClass(attrs.activate);
					element.text(attrs.activate).next().addClass(attrs.activate);


					$('#movies').addClass('active');

					//if one hasnt been selected
					$('#movies').find('.large-3').each(function(){
						if($(this).find('p').hasClass('selected') !== true) {

							$(this).animate({
								opacity: 0.3 
							}, 400, 'swing', function(){
								
								$(this).find('p').addClass('dontClick');

							});



						} else {

							//element.removeClass(attrs.activate).text(scope.app.message);
							
						}
					});

					function flip(){
						var nuhi = element.parents('.flip-container').height();

						element.parents('.flip-container').addClass('flip');
						element.parents('.flipper').find('.back').css('height', nuhi);
					}
					

					setTimeout(flip, 400)


				} else {

					element.removeClass(attrs.activate).text(scope.app.message);
					element.next().removeClass(attrs.activate);

					//if one has been selected
					$('#movies').find('.large-3').each(function(){
							$(this).animate({
								opacity: 1
							}, 400, 'swing', function(){
								//$(window).scrollTo('#submit', 800, 'swing', {offset:-100});
								$(this).find('p').removeClass('dontClick');
							});
					});
				}

			//}
		
		});


	}
});



/**end angular**/