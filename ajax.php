<?php
/////////////////////////////////////////
// @ Example usage of the HK API
// @ 15th February 2013
// @ Author: Ashley Ford
// @ ashley@harkable.com
////////////////////////////////////////
include('class/HkCompAPI.class.php');

$competition = new HkCompAPI();

  if(isset($_POST)){

      $validation = array(
  
        //@ Format of the array is as follows:
        //@ Input Value Name
        //@ Error Message
        //@ Required, valid_email, over_18, captcha (value must be blank)     
        
      array('fname',    "Please enter your first name",          'required'),
      array('lname',    "Please enter your last name",           'required'),
      //array('dob',      "This promotion is open to users over 18!",  'over_18'),
      array('email',    "Please enter a valid email address",      'valid_email'),
      //array('terms',    "Please Accept the Terms &amp; Conditions",  'required'),
      array('subscribe',  "Please Accept the Terms &amp; Conditions",  'required'),
      //array('_captcha', "We're not sure you're a human...",      'captcha')
  
    );  
    
    // validate for errors
    $errors = $competition->validate($validation);
    
    // validate returns FALSE if no errors are found
    if($errors===FALSE){
      // save the data via the api call with the campaign name
          $save = $competition->save('ign_test');
                    
          if(isset($save['_auth']) && $save['_auth']=='failed'){
            echo 'Error Authenticating With the API';           
          }
             
            // if the data was saved successfully   
          if(isset($save['_saved'])==TRUE){
              echo '1';
          }else{
              echo '0';
          }
                     
    }else{

      echo '0';

    }
}